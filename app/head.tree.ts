import {Component, OnInit, Input} from '@angular/core';
@Component({
    selector: 'head-tree',
    moduleId: module.id,
    templateUrl: 'head.tree.html',
    styleUrls: ['./head.tree.css'] 
})
export class HeadTree{
    @Input() name:string;
    @Input() children: Array<NodeTree>;
    @Input() parents: NodeTree;
    @Input() depth: number;
    addchild(){
        let newNode = new NodeTree();
        newNode.depth=this.depth+1;
        newNode.name=this.parents.name+newNode.depth+(+this.children.length+1);
        newNode.children=[];
        newNode.parents=this.parents
        this.children.push(newNode)
        console.log(this)
    }
}
/*interface SomeComponentInterface{
    name: string;
}*/
class NodeTree{
    name: string;
    children: Array <NodeTree>;
    parents: NodeTree;
    depth: number;
}