import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HeadTree} from './head.tree';
import {NodeTree} from './child.tree' 
@NgModule({
    imports: [BrowserModule,
              FormsModule],
    declarations: [AppComponent, 
                   HeadTree,
                   NodeTree],
    bootstrap: [AppComponent]
})
export class AppModule{}