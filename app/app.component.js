"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var head_tree_1 = require('./head.tree');
//import {HeadTree} from './head.tree'
var AppComponent = (function () {
    function AppComponent() {
        this.name = '';
        this.massive = Massive;
    }
    AppComponent.prototype.addNewNode = function () {
        if (!this.name.trim()) {
            return;
        }
        var NewTree = new head_tree_1.HeadTree();
        NewTree.depth = 0;
        NewTree.name = this.name;
        NewTree.parents = NewTree;
        NewTree.children = [];
        Massive.push(NewTree);
        this.name = '';
        console.log(Massive);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            moduleId: module.id,
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
var Massive = [];
//# sourceMappingURL=app.component.js.map