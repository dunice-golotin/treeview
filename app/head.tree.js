"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var HeadTree = (function () {
    function HeadTree() {
    }
    HeadTree.prototype.addchild = function () {
        var newNode = new NodeTree();
        newNode.depth = this.depth + 1;
        newNode.name = this.parents.name + newNode.depth + (+this.children.length + 1);
        newNode.children = [];
        newNode.parents = this.parents;
        this.children.push(newNode);
        console.log(this);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], HeadTree.prototype, "name", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], HeadTree.prototype, "children", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', NodeTree)
    ], HeadTree.prototype, "parents", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], HeadTree.prototype, "depth", void 0);
    HeadTree = __decorate([
        core_1.Component({
            selector: 'head-tree',
            moduleId: module.id,
            templateUrl: 'head.tree.html',
            styleUrls: ['./head.tree.css']
        }), 
        __metadata('design:paramtypes', [])
    ], HeadTree);
    return HeadTree;
}());
exports.HeadTree = HeadTree;
/*interface SomeComponentInterface{
    name: string;
}*/
var NodeTree = (function () {
    function NodeTree() {
    }
    return NodeTree;
}());
//# sourceMappingURL=head.tree.js.map