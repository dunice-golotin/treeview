import { Component } from '@angular/core';
import { HeadTree } from './head.tree'
//import {HeadTree} from './head.tree'
@Component({
    selector: 'my-app',
    moduleId: module.id,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    name:string='';
    addNewNode(){
        if(!this.name.trim()){
            return;
        }
        
        let NewTree: HeadTree= new HeadTree();
        NewTree.depth=0;
        NewTree.name=this.name;
        NewTree.parents=NewTree;
        NewTree.children=[];
        Massive.push(NewTree);
        this.name='';
        console.log(Massive)
    }
    massive=Massive;
}
let Massive: Array<HeadTree>=[]